package com.webservices.uppgift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class UppgiftApplication {

    @Bean
    public WebClient getWebClient(){
        return WebClient.create();
    }

    public static void main(String[] args) {
        SpringApplication.run(UppgiftApplication.class, args);
    }

}
