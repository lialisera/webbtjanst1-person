package com.webservices.uppgift.controllers;

import com.webservices.uppgift.dto.CreatePerson;
import com.webservices.uppgift.dto.GroupDTO;
import com.webservices.uppgift.dto.PersonDTO;
import com.webservices.uppgift.entity.Person;
import com.webservices.uppgift.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * WebTjänst 1 - Personer
 * Rest
 * Swagger (ej krav)
 * CRUD Person (ID, Förnamn, Efternamn, Grupper[lista - hämtas från Webtjänst 2])
 *  Använder Webtjänst 2
 *  create person->done
 *  read person-> done
 *  update person->done
 *  delete person->done
 */
@RestController
@RequestMapping("/api/person")

@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonController {
    @Autowired
    PersonService personService;

    @Autowired
    WebClient webClient;

    private static final String CODESECURITY = "123";

    @RequestMapping(value = "allPerson", method = RequestMethod.GET)
    public List<PersonDTO> getPersons() {
        return personService.getPersons().map(this::toPersonDTO).collect(Collectors.toList());
    }

    @PostMapping()
    public PersonDTO createPerson(@RequestBody CreatePerson createPerson) throws Exception {
        return toPersonDTO(personService.createPerson(createPerson));
    }
    @PutMapping("/{personId}")
    public PersonDTO updatePerson(@PathVariable("personId") String personId, @RequestBody CreatePerson createPerson) throws Exception {
        return toPersonDTO(personService.updatePerson(personId,createPerson));
    }

    @DeleteMapping("/{personId}")
    public void deletePerson(@RequestHeader(value = "personId", required = true) String personId) throws Exception {
        personService.deletePerson(personId);
    }

    /*@DeleteMapping("/{memberId}")
    public void deleteMemeber(@RequestHeader(value = "memberId", required = true) String memberId) throws Exception {
        deleteMemberByMemberId(memberId);
    }

    @DeleteMapping("/{groupId}")
    public void deleteGroup(@RequestHeader(value = "groupId", required = true) String groupId) throws Exception {
        deleteGroupByGroupId(groupId);
    }*/

    private PersonDTO toPersonDTO(Person person) {
        List<GroupDTO> groups = findAll().collect(Collectors.toList());
        return new PersonDTO(person.getId(),
                person.getFirstName(),
                person.getLastName(),
                groups
        );
    }

    private GroupDTO deleteMemberByMemberId(String memberId){
        return webClient.delete()
                .uri("http://localhost:8084/api/group/delete/"+memberId)
                .header("codekey",CODESECURITY)
                .retrieve()
                .bodyToMono(GroupDTO.class).block();
    }

    private Mono<Void> deleteGroupByGroupId(String groupId){
        return webClient.delete()
                .uri("http://localhost:8084/api/group/delete/"+groupId)
                .header("codekey",CODESECURITY)
                .retrieve()
                .bodyToMono(void.class);
    }

    private Stream<GroupDTO> findAll() {
        return webClient.get()
                .uri("http://localhost:8084/api/group")
                .header("codekey",CODESECURITY)
                .retrieve()
                .bodyToFlux(GroupDTO.class).toStream();
    }
}
