package com.webservices.uppgift.entity;

import com.webservices.uppgift.dto.GroupDTO;
import lombok.Data;

import javax.persistence.ElementCollection;
import java.util.ArrayList;
import java.util.List;

@Data
public class Person {
    private String id;
    private String firstName;
    private String lastName;

    @ElementCollection
    protected List<String>Groups;


    public Person(String id, String firstName, String lastName){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        Groups = new ArrayList<>();
    }
    public void addGroup (GroupDTO group){
        Groups.add(group.getId());
    }
}
