package com.webservices.uppgift.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GroupDTO {
    String id;
    String name;

    @JsonCreator
    public GroupDTO(@JsonProperty("id") String id,
                    @JsonProperty("name") String name){
        this.id=id;
        this.name=name;
    }
}
