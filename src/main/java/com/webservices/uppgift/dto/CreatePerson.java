package com.webservices.uppgift.dto;

import lombok.Data;

@Data
public class CreatePerson {
    String firstName;
    String lastName;
}

