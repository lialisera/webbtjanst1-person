package com.webservices.uppgift.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class PersonDTO {

    String id;
    String firstName;
    String lastName;
    protected List<GroupDTO> groups;

    public PersonDTO(String id, String firstName, String lastName, List<GroupDTO> groups){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groups = groups;
    }
}

