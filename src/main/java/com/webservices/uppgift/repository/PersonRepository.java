package com.webservices.uppgift.repository;

import com.webservices.uppgift.entity.Person;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class PersonRepository {
    Map<String, Person> persons = new HashMap<>();

    public Collection<Person> findAll() {
        return persons.values();
    }

    public void save(Person person) {
        persons.put(person.getId(), person);
    }

    public Person findById(String tokenId) {
        return persons.values().stream().filter(t -> t.getId().equals(tokenId)).findFirst().get();
    }

    public void delete(Person person) {
        if(!persons.isEmpty())
            persons.remove(person.getId());
    }
}

