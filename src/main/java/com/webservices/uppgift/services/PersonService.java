package com.webservices.uppgift.services;

import com.webservices.uppgift.dto.CreatePerson;
import com.webservices.uppgift.dto.GroupDTO;
import com.webservices.uppgift.entity.Person;
import com.webservices.uppgift.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PersonService {

    PersonRepository personRepository;

    public Stream<Person> getPersons() {
        return personRepository.findAll().stream();
    }

    public Person createPerson(CreatePerson createPerson) {
        Person person=new Person(UUID.randomUUID().toString(),createPerson.getFirstName(),createPerson.getLastName());
        personRepository.save(person);
        return person;
    }

    public Person updatePerson( String personId, CreatePerson createPerson) {
        Person oldPerson= personRepository.findAll().stream().filter(g -> g.getId().equals(personId)).findFirst().get();
        oldPerson.setFirstName(createPerson.getFirstName());
        oldPerson.setLastName(createPerson.getLastName());
        personRepository.save(oldPerson);
        return oldPerson;
    }

    public void deletePerson(String personId) {
        Person person = personRepository.findById(personId);
        personRepository.delete(person);
    }
}

